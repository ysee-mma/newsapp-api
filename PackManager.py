# coding: utf-8   coding utf-8  coding=utf-8

import endpoints
import FactoryParse
from Manager import Manager
from ParsingException import ParsingException
from tools import tools
from datetime import datetime, timedelta
from pytz import timezone
import pytz
import sys
import json

class PackManager(object):
	'''
	switching packed data between parsing modules 
	 
	'''
	this_module_name = str(sys.modules[__name__])
	count=-1
	Source_Name ="" # twitter.com_JonnyBones -> twitter.com
	Source_param = "" #JonnyBones
	data_by_source = "" # data to parse from source

	raw_data= "" # raw data from client

	clienttime = "" # '2008-12-12 19:21:10'
	clienttimezone = "" # '+3' to UTC
	clientlocale = "" # 'ru'
	clientip = ""
	
	articles = []



	response_json_news_temp =""
	response_sourcehash_temp = ""
	response_json_news_temp = ""



	ip_another_clients = ""

	
	
	def __init__(self, data_to_parse, time,timezone,locale,client_ip):
		'''
		call from API: PackManager(request.requestbody, request.time, request.timezone, request.locale)
		Constructor
		'''


		self.clienttime = time
		self.clienttimezone = timezone
		self.clientlocale = locale
		self.clientip = client_ip

		self.raw_data = tools.safe_str(data_to_parse)




	def GetIp(self):
		#ip_as_str = ', '.join('%s' %(k) for k in ip_another_clients)
		#dict_ip_print = ', '.join('%s : %s' %(k,self.HashesAndIPDictionary[k]) for k in self.HashesAndIPDictionary.keys())
		ip_as_str = ', '.join(self.ip_another_clients)
		return ip_as_str





	def Parse(self):
		this_function_name = str(sys._getframe().f_code.co_name)
		try:
			# requestbody  = messages.StringField(5) # html-string #to do: json-array
			try:

				json_arr = json.loads(self.raw_data)
				dict_result = {}
				list_of_jsons = []

				#sources = "" 

				for json_source in json_arr:
					try:
						# ignoring all sort of error 
						source = json_source['source'].replace('https://','').replace('https://','')
						#sources = sources + "  " +str(source)
						#source = tools.GetUpTo2Domain(source)
							
						source_unslashed = source.replace('/','_')
						requestbody = json_source['html']
						manager_ = Manager(requestbody, source_unslashed, self.clienttime, self.clienttimezone, self.clientlocale, "", self.clientip)
						json_news_list_unescaped = manager_.Parse()
						
						dict_result['source'] = json_source['source']
						dict_result['news'] = json_news_list_unescaped

						list_of_jsons.append(dict_result.copy())
					except Exception as e2:
						pass

				
				json_data = json.dumps(list_of_jsons)

				#ParsingException.GAE("get parsed news %s " %(str(len(json_data))))
				return json_data
			except Exception as e2:
				message = 'PackManager unable to get jsons from given data: "%s" . %s' % (self.Source_Name,str(e2))
				ParsingException.GAE(message)
			return json_data
			#return "[{\"source\":\"twitter.com/jonnybones\",\"news\":[{\"m\":1519320609000, \"d\":\"2018-02-22 17:30:09\", \"i\":\" 1 jonnybones Yesterday's strength work out went great! Thank you @jonnybones for the motivation to lift heavy https://www.instagram.com/p/Bfgc32mn8Wn/\", \"l\":\"/brownbearC/status/966726820333871109\", \"p\":\"NoPic\",\"e\":\"NoArticleText\",\"y\":\"TWITTER\"},{\"m\":1519320609003, \"d\":\"2018-02-22 17:30:13\", \"i\":\" 3 jonnybones Yesterday's strength work out went great! Thank you @jonnybones for the motivation to lift heavy https://www.instagram.com/p/Bfgc32mn8Wn/\", \"l\":\"/brownbearC/status/966726820333871109\", \"p\":\"NoPic\", \"e\":\"NoArticleText\",\"y\":\"TWITTER\"}]},{\"source\":\"twitter.com/dc_mma\",\"news\":[{\"m\":1519320609002, \"d\":\"2018-02-22 17:30:12\", \"i\":\" dc_mma 2 Yesterday's strength work out went great! Thank you @jonnybones for the motivation to lift heavy https://www.instagram.com/p/Bfgc32mn8Wn/\", \"l\":\"/brownbearC/status/966726820333871109\", \"p\":\"NoPic\", \"e\":\"NoArticleText\",\"y\":\"TWITTER\"},{\"m\":1519320609004, \"d\":\"2018-02-22 17:30:14\", \"i\":\" dc_mma 4 Yesterday's strength work out went great! Thank you @jonnybones for the motivation to lift heavy https://www.instagram.com/p/Bfgc32mn8Wn/\", \"l\":\"/brownbearC/status/966726820333871109\", \"p\":\"NoPic\", \"e\":\"NoArticleText\",\"y\":\"TWITTER\"}]}]"
		except Exception as e1:
			message = 'PackManager unable to parse: "%s" . %s' % (self.Source_Name,str(e1))
			ParsingException.GAE(message)





