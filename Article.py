# -*- coding: utf-8 -*-
import sys
from ParsingException import ParsingException
from tools import tools
import endpoints

class Article(object):
	'''
	Object-type Article
	'''

	this_module_name = str(sys.modules[__name__])
	
	title = "NoTitle"
	link = "NoLink"
	pic = "NoPic"
	date = "NoDate"
	date_in_milliseconds= 0
	date_as_date = None
	text = "NoArticleText"
	type = None
	json_element = ""

	def __init__(self, _title, _link, _pic, _date_in_milliseconds, date_as_date, _text, _type):
		'''
		Create Article-object with: title, link, pic, date in milliseconds, date_as_date, text, type
		'''
		try:
			if (_title):
			#if (title!=''):
			#if (tools.safe_str(_title)!=""):
				self.title=tools.safe_str(_title).replace('"','\\"')
			if (_link):
				self.link=_link
			if (_pic):
				self.pic=_pic
			if (_text):
				self.text=tools.safe_str(_text)#.replace('"','\\"')
			
			self.type = _type
			
			
			self.date_in_milliseconds = _date_in_milliseconds
			self.date_as_date = date_as_date
	
			self.json_element = self.BuildJSON()
			
		except Exception as E:
			#incoming_data = '"date_in_milliseconds":'+str(_date_in_milliseconds)+', "date_as_date":"'+str(date_as_date)+'", "title":"'+_title.encode('utf-8')+'", "link":"'+str(_link)+'", "pic":"'+str(_pic)+'", "text":"'+_text.encode('utf-8')+'","type":"'+str(type)
			incoming_data = '"link":"'+str(_link)+'"'
			message = "An Error occured while creating object Article. %s . Incoming data = [%s]" % (str(E), incoming_data)
			ParsingException.GAE(message)
			#incoming_data = '"date_in_milliseconds":'+str(_date_in_milliseconds)+', "date_as_date":"'+str(date_as_date)+'", "title":"'+str(_title)+'", "link":"'+str(_link)+'", "pic":"'+str(_pic)+'", "text":"'+str(_text)+'","type":"'+str(type)
			#ParsingException.Write("An Error occured while creating object Article "+ str(E), incoming_data, str(sys.modules[__name__]))

	def GetDateAsLong(self):
		return self.date_in_milliseconds  
		
	def GetALL(self):
		try:
			return (self.title).encode('utf-8')+"\t"+str(self.date_as_date)+"\t"+str(self.link)+"\t"+str(self.pic)+"\t"+(self.text).encode('utf-8')+"\t"+str(self.type)
		except Exception as e:
			message = "An Error occurred while concatenating all article fields to one string: %s \n" %e 
			ParsingException.GAE(message)
			#sys.stdout.write("An Error occurred while concatenating all fields to one string: %s \n" %e )
			#sys.stdout.flush()

			
	def BuildJSON(self):
		'''
		Сокращаем максимально длину служебных данных, поскольку в каждом ответе на запрос мы пишем в среднем эти данные 20 раз
		date_in_milliseconds == m
		date_as_date == d
		title = i
		link == l
		pic == p
		text == e
		type == y
		'''
		this_function_name = sys._getframe().f_code.co_name
		try:

			return '{"m":'+str(self.date_in_milliseconds)+', "d":"'+str(self.date_as_date)+'", "i":"'+self.title+'", "l":"'+str(self.link)+'", "p":"'+str(self.pic)+'", "e":"'+self.text+'","y":"'+str(self.type)+'"}'
			'''
			return '{"date_in_milliseconds":'+str(self.date_in_milliseconds)+', "date_as_date":"'+str(self.date_as_date)+'", "title":"'+str(self.title)+'", "link":"'+str(self.link)+'", "pic":"'+str(self.pic)+'", "text":"'+str(self.text)+'","type":"'+str(self.type)+'"}'
			'''
		except Exception as e:
			#sys.stdout.write("An Error occurred while building json for article: %s \n" %e )
			#sys.stdout.flush()
			message = "An Error occurred while building json from article-object. %s at %s %s \n" % (str(e), this_function_name, self.this_module_name)
			#ParsingException.Write("Ошибка определения контейнера новостей по селектору ["+selector+"]. " + str(e), str(soup), str(sys.modules[__name__]))
			ParsingException.GAE(message)
			
	def GetJSON(self):
		return self.json_element
		
	
	

