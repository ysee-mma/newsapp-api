# -*- coding: utf-8 -*-

from Parse_blank import Parse_blank_class
from datetime import datetime
import locale
import sys
from ParsingException import ParsingException
import Logging
#from tools import tools

class Parse_Twitter(Parse_blank_class):
	'''
	parsing twitter.com posts
	
	done:
	- tweet
	- текстовый ретвит с комментарием
	- ретвит на видос без комментария

	to do:
	- не найден текст твита, на который дан комментарий. твит может быть как твитом отдельным, так и реплаем автору
	- не найдент текст твита1, на который дан ответ2. Твит1 является ответом на полее старый твит автора
	
	- лишний title_rt_2 если есть ретвит с текстом и картинкой, на который дан комментарий
	- лишний title_rt_2 если дан ответ на медиа-твит с текстом

	'''
	this_module_name = str(sys.modules[__name__])
	url_domain = "twitter.com"
	reply_path = "with_replies"
	
	article = None
	  

	#*********************************************************************************************************************************************************************************************************
	
	
	def find_Articles(self, soup):
		this_function_name = str(sys._getframe().f_code.co_name)
		selector = "ol[id='stream-items-id'] li[data-item-type=tweet]" # selector = "div[id=timeline] li[data-item-type=tweet]"
		selector2= "ol[id='stream-items-id'] li div.content"#"ol[id=stream-items-id] div[class=js-tweet-text-container]"
		selector3= "ol[id='stream-items-id'] div.content"
		try:
			res = []
			
			res = soup.select(selector)
			if len(res)!=0:
				return res
			#raise ParsingException.GAE('%s articles'% (len(res)))
			
			res = soup.select(selector2)
			if len(res)!=0:
				return res

			res = soup.select(selector3)
			if len(res)!=0:
				return res
				#ParsingException.Write("Ошибка определения контейнера новостей по селектору ["+selector+"]. Пустой список", str(soup), str(sys.modules[__name__]))
			
			ParsingException.GAE('EMPTY article Container')
			
			#Logging.Log.Write("сообщений найдено: "+str(len(res)))
			
			return res
		except Exception as e:
			message = 'Failed to find news container by selector1: [%s] and selector2 [%s] with error %s at %s %s' % (selector,selector2,str(e),this_function_name,self.this_module_name)
			#ParsingException.Write("Ошибка определения контейнера новостей по селектору ["+selector+"]. " + str(e), str(soup), str(sys.modules[__name__]))
			ParsingException.GAE(message)
			#self.LogError("Не смогли в json -Объекте найти ни одной записи. Селектор ["+selector+"]."+ str(e)+" \n", str(soup), str(sys.modules[__name__]))
		 
	def find_Pic(self, article):
		return ""
	'''
		selector = str("img")
		attribute = str("src")
		try:
			pic = article.select(selector)[0].get(attribute);
			try:
				pic = pic.split('.vresize')[0]+".jpg"
			except Exception as e:
				pass
			return pic
		except Exception as e:
			#self.LogError("Не смогли выделить картинку статьи на странице.  Селектор ["+selector+"] аттрибут ["+attribute+"]."+ str(e)+"\n", str(article), str(sys.modules[__name__]))
			return ""
			'''
		
		
	def find_Link(self, article):
		this_function_name = str(sys._getframe().f_code.co_name)
		selector = "small.time a"
		attribute = "href"
		try:
			link = article.select(selector)[0].get(attribute) # http://www.foxsports.com/ufc/story/ufc-hidalgo-poirier-vs-johnson-post-fight-thoughts-091816
			
			#Logging.Log.Write("ссылка найдена: "+str(link))
			
			if not link:
				ParsingException.GAE("Ошибка определения ссылки. Селектор ["+selector+"] аттрибут ["+attribute+"]. Пустая ссылка")
				#ParsingException.Write("Ошибка определения ссылки. Селектор ["+selector+"] аттрибут ["+attribute+"]. Пустая ссылка", str(article), str(sys.modules[__name__]))
			'''
			исключим из экономии трафика
			if self.url_domain not in link:
				link = self.url_domain+link
			
			if "http://www." not in link:
				link = "http://www."+link
			''' 
			return link
		except Exception as e:
			message = 'Failed to find a link by selector: [%s] with error %s. Total articles: %s at %s %s' % (selector,str(e),len(self.article),this_function_name,self.this_module_name)
			ParsingException.GAE(message)

			#ParsingException.GAE("Ошибка определения ссылки. Селектор ["+selector+"] аттрибут ["+attribute+"]."+ str(e))
			#ParsingException.Write("Ошибка определения ссылки. Селектор ["+selector+"] аттрибут ["+attribute+"]."+ str(e), str(article), str(sys.modules[__name__]))
			#self.LogError("Не смогли найти ссылку на статью. Селектор ["+selector+"] ."+ str(e)+"\n", str(article), str(sys.modules[__name__]))
			return ""
			
		
		
	def find_Date(self, article):
		this_function_name = str(sys._getframe().f_code.co_name)
		selector = "small.time a"
		selector2 = "a[class='tweet-timestamp js-permalink js-nav js-tooltip']"
		attribute = "title" # SEP 17, 10:06p ET -- SEP 18, 1:01a ET
		attribute2 = "data-original-title"  #11:10 - 16 февр. 2018 г.
		try:
			self.article = article
			date = article.select(selector2) 
			#Logging.Log.Write("дата найдена: "+str(date))
			if (not date) or (date==None) or (date==''):
				message = 'Empty date by selector: [%s]' % (selector2)
				ParsingException.GAE(message)
				#ParsingException.Write("Ошибка выделения даты новости. Селектор ["+selector+"]. Пустая дата", str(article), str(sys.modules[__name__]))
			date = date[0].get(attribute) # SEP 17, 10:06p ET -- SEP 18, 1:01a ET
			return date
		except Exception as e:
			message = 'An Error occurred while finding a date with error %s.Total articles: %s  at %s %s' % (str(e),len(self.article), this_function_name, self.this_module_name)
			ParsingException.GAE(message)
			#ParsingException.Write("Ошибка выделения даты новости. Селектор ["+selector+"]. " + str(e), str(article), str(sys.modules[__name__]))
			#self.LogError("Не смогли найти дату статьи. Селектор ["+selector+"]."+ str(e)+" \n", str(article), str(sys.modules[__name__]))
			return ""
		
		
		
	def find_Title(self, article):
		status = ""

		this_function_name = str(sys._getframe().f_code.co_name)
		selector1 = "div.js-tweet-text-container > p"
		selector2 = "p"
		selector_reply  = "div > div > div > div > div > div > div > div"
		selector4_comment_on_retweet = "div p"
		selector4_comment_on_retweet_tail_to_cut = "div p a"
		selector_comm_on_reply = "div.ReplyingToContextBelowAuthor"


		selector_raw = "div > div > div > div > div > div"

		selector4_comment_on_retweet_raw =  "div > div > div > div > div > div"
		try:
			title = ""
			title_rt = ""
			title_com_rt = ""

# текст обычного твита			
			try:
				#title = article.select(selector2)[0].get_text().strip().encode('utf-8')
				title = article.select(selector1)[0].get_text().strip()
				status = "[title по selector1]"
			except Exception as e:
				message = 'no title'
				title=""
			
			if (not title):
			#if (title==''):
				try:
					status = status + "[title по selector2]"
					#title = article.select(selector1)[0].get_text().strip().encode('utf-8')
					title = article.select(selector2)[0].get_text().strip()
					if not title:
						status = status + "[no title by selector2]"
						message = 'Failed to find a title by selector2: [%s] and selector2 [%s] empty title' % (selector1,selector2)
						ParsingException.GAE(message)
					else:
						title = "заголовок:"+ title + " "
						status = status + "[ok title by selector1]"
				except Exception as e:
					message = 'no title'
					title=""

# РАБОТАЕТ: текст комментария на ретвит 
			try:
				#title_com_rt = article.select(selector4_comment_on_retweet)[0].get_text().strip().encode('utf-8')
				title_com_rt = article.select(selector4_comment_on_retweet)[1].get_text().strip()
				title_com_rt_tail = article.select(selector4_comment_on_retweet_tail_to_cut)[0].get_text().strip()
				title_com_rt = title_com_rt.replace(title_com_rt_tail,"")

				#title_com_rt = "[comment on retweet]: " + title_com_rt
				status = status + "[ОК comment by selector4]"
			except Exception as e:
				message = '[no comment on retweet]'
				title_com_rt=""
			
# РАБОТАЕТ : текст реплая автору твиттера, на что дан комментарий		
			try:
				title_reply = ""
				#title_reply = "[title_reply:]"+article.select(selector_reply)[ 4 ].get_text().strip()
				title_reply = article.select(selector_reply)[ 4 ].get_text().strip()
					
			except Exception as e:
				pass

# РАБОТАЕТ : текст ретвита на который дан комментарий			
			try:
				if (not title_reply):
					title_plainretweet = ""
					#title_plainretweet = "[title_plainretweet:]"+article.select(selector_reply)[ 3 ].get_text().strip()
					title_plainretweet = article.select(selector_reply)[ 3 ].get_text().strip()
				else:
					title_plainretweet = ""
			except Exception as e:
				pass				

# работает : текст реплая с гифкой на который дан комментарий		   
			try:
				
				title_reply_gif = ""
				title_reply_gif = article.select(selector_reply)[ 5 ].get_text().strip()
				#title_reply_gif = "[title_reply_gif:]"+article.select(selector_reply)[ 5 ].get_text().strip()
				
			except Exception as e:
				pass
# текст ретвита			
			try:



				title_rt = title_reply + title_plainretweet + title_reply_gif
				

				'''
				cand_list = ""
				_len = len(article.select(selector3_retweet))
				i = 0
				for candidate in article.select(selector3_retweet):
					i = i + 1
					try:
						if (candidate.get_text().strip()):
							cand_list = cand_list + " ["+ str(i)+ " " +candidate.get_text().strip()+"]"
					except Exception, e:
						pass
				message = '[no retweet]'+cand_list
				title_rt=message
				'''
			except Exception as e:
				title_rt=""
				
				
				


# сбор текстов в формат			
			try:
				title_combined = ""
				if (title_com_rt): 
					title_combined = title_com_rt + " [on tweet]:" + title_rt

				if (not title_combined): 
					title_combined = title
				#return title_combined
			except Exception as e:
				message = 'no title_combined'
			


			return title_combined
		except Exception as e:
			message = 'An Error occurred [%s] while finding a title. Total articles: %s. At %s %s' % (str(e), self.count, this_function_name, self.this_module_name)
			ParsingException.GAE(message)
			#ParsingException.Write("Ошибка определения заголовка новости. Селектор ["+selector+"]. " + str(e), str(article), str(sys.modules[__name__]))
			#self.LogError("Не смогли найти заголовок статьи. Селектор ["+selector+"]."+ str(e)+" \n", str(article), str(sys.modules[__name__]))
			return "" 


	def find_Date_as_MS(self):
		this_function_name = str(sys._getframe().f_code.co_name)
		selector = "small.time a span"
		attribute = "data-time-ms" # 1516817556000
		try:
			date = self.article.select(selector) 
			#Logging.Log.Write("дата найдена: "+str(date))
			if (not date) or (date==None) or (date==''):
			#if (not date) or (date==None) or (date==''):
				message = 'Failed to find a date as milliseconds by selector: [%s] empty locator' % (selector)
				ParsingException.GAE(message)
				#ParsingException.Write("Ошибка выделения даты новости. Селектор ["+selector+"]. Пустая дата", str(article), str(sys.modules[__name__]))
			date = date[0].get(attribute) # SEP 17, 10:06p ET -- SEP 18, 1:01a ET
			return long(float(date))
		except Exception as e:
			message = 'Failed to find a date as milliseconds by selector: [%s] with error %s.Total articles: %s at %s %s' % (selector, str(e),len(self.article), this_function_name, self.this_module_name)
			ParsingException.GAE(message)
			#ParsingException.Write("Ошибка выделения даты новости. Селектор ["+selector+"]. " + str(e), str(article), str(sys.modules[__name__]))
			#self.LogError("Не смогли найти дату статьи. Селектор ["+selector+"]."+ str(e)+" \n", str(article), str(sys.modules[__name__]))
			return ""

	
	def get_Date_as_Long(self,date_with_year):
		this_function_name = str(sys._getframe().f_code.co_name)
		date_format = '%H:%M %d %B %Y %z'
		date_alt = None
		try:
			try:
				date_alt = self.find_Date_as_MS()
				return date_alt
			except Exception as e:
				pass


			date_0 = None
			try:
				date_string = date_with_year.encode('utf-8') # 19:34 - 25 дек. 2016 г.
				date_string = date_string.replace(" г.","").replace(" - "," ").replace(".","")
				date_string = date_string.replace("янв","январь").replace("февр","февраль").replace("мар","март").replace("апр","апрель").replace("мая","май").replace("июн","июнь").replace("июл","июль").replace("авг","август").replace("сент","сентябрь").replace("окт","октябрь").replace("нояб","ноябрь").replace("дек","декабрь")
				date_string = date_string + " -0800"  
				date_string.strip().encode('utf-8')
				locale.setlocale(locale.LC_ALL, 'ru')
				#date_string = date_string.replace("янв.","январь").replace("рта","рт").replace("ая","ай").replace("ня","нь").replace("та","т") 
				
				#Logging.Log.Write("установили локаль Рус: ["+str(date_string)+"]")
				
				date_0 = datetime.strptime(date_string.encode('utf-8'), date_format)
				
				#Logging.Log.Write("конвертировали в дату: "+str(date_0))
				
			except Exception as e1:
				message = 'Failed to parse Date by format: [%s] with error: %s' % (date_format,str(e1))
				ParsingException.GAE(message)
				#ParsingException.Write("Ошибка распознавания найденной даты новости  по шаблону ['%H:%M %d %B %Y %z']. " + str(e1), str(date_0), str(sys.modules[__name__]))
			
			
			#milliseconds = (date.utctimetuple().tm_yday * 24 * 60 * 60 + date.seconds) * 1000 + date.microseconds / 1000.0
			return self.get_Milliseconds(date_0)
			#return integer_timestamp
		except Exception as e:
			message = 'Failed to converting to milliseconds parsed Date with error: %s. Total articles: %s.  at %s %s' % (str(e), len(self.article), this_function_name, self.this_module_name)
			ParsingException.GAE(message)
			#ParsingException.Write("Ошибка перевода в миллисекунды найденной и распарсенной даты. " + str(e), str(date_0), str(sys.modules[__name__]))
			#self.LogError("Не смогли распознать выделенную дату статьи. шаблон [%b %d, %I:%M %p %z]."+ str(e)+" \n", str(date_no_year), str(sys.modules[__name__]))
			#sys.stdout.write("ошибка парсинга строковой даты для представления в виде даты. Модуль Parse_FoxSports_v_1: %s\n" %e )
			#sys.stdout.flush() 
			return 0
			

	
	
	