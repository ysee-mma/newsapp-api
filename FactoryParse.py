# coding: utf-8   coding utf-8  coding=utf-8

from Parse_Twitter import Parse_Twitter
from Parse_Instagram import Parse_Instagram
from tools import tools
from enum import Enum
from ParsingException import ParsingException


SourcesSupportedDict = None

Type = Enum('Type', 'INSTAGRAM FOXSPORTS RSPORT TASS UNDEFINE MATCHTV VK WMMAA ESPN TWITTER')
SourcesSupportedDict = {'twitter.com':'twitter', 'instagram.com':'instagram'}


def GetSourceByName(source):
	try:
		if (source in SourcesSupportedDict):
			if (SourcesSupportedDict[source]=="twitter"):
				return Parse_Twitter
			if (SourcesSupportedDict[source]=="instagram"):
				return Parse_Instagram
		else:
			dict_print = ', '.join('%s' %(k) for k in SourcesSupportedDict)
			ParsingException.GAE("Unsupported source [%s]. List of supported: [%s]" %(source, dict_print))
	except Exception, e:
		ParsingException.GAE("Failed in parser factory for source [%s]. %s" %(source,str(e)))


def GetSupported():
	return SourcesSupportedDict


def GetSupportedAsStringOfList():
	dict_print = ', '.join('%s' %(k) for k in SourcesSupportedDict)
	dict_print = '['+dict_print+']'
	return dict_print


def IsSupported(source_url):
	domain = tools.GetUpTo2Domain(source_url)
	if (domain in SourcesSupportedDict):
		return str(True)
	else:
		return str(False)



def GetType(x):
	try:
		res = ""
		for _type in Type:
			if (str(_type).replace("Type.","").lower() in x):
				res = str(_type).replace("Type.","") 
				return res
				
		return str(Type.UNDEFINE).replace("Type.","")
	except Exception as e:
		ParsingException.GAE("An Error occured while defining Type of given news source %s" % str(e))
