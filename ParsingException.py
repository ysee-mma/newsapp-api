# -*- coding: utf-8 -*-
from datetime import datetime
import sys
import Logging
import endpoints

class ParsingException(object):
	'''
	logging parsing exception
	Example: ParsingException.ParsingException.Write("Ошибка создания объекта", incoming_data, str(sys.modules[__name__]))
	Example: ParsingException.ParsingException.GAE("Ошибка создания объекта")
	'''
			
	def Write(ErrorMessage, data, modulename):
		_path = "_Logs/_Errors/"
		date = str(datetime.utcnow())
		file_name =  (date+".txt").replace(":","-")
		sys.stdout.write("пишем ошибку ПАРСИНГА в файл: %s \n" %file_name )
		sys.stdout.flush()
		try:
			Logging.Log.Write("ОШИБКА ПАРСЕРА ["+ErrorMessage+"] данные записаны в файл ["+_path +file_name+"]" )
			f = open(_path +file_name, 'w')
			try:
				f.write("Time : " + date + "\n")
				f.write("Module Name : " + modulename + "\n")
				f.write("ErrorMessage : " + ErrorMessage + "\n")
				f.write("data : " + str(data.encode('utf-8')) + "\n")
				
			except Exception as E:
				Logging.Log.Write("Ошибка записи в файл данных о сломанном парсинге " + str(E) )
								
			f.close()
		except Exception as FE:
			Logging.Log.Write("Ошибка записи в файл данных о сломанном парсинге " + str(FE) )
				
	def GAE(ErrorMessage):
		raise endpoints.InternalServerErrorException(ErrorMessage)
	
	Write = staticmethod(Write)
	GAE   = staticmethod(GAE)