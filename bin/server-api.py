# coding=utf-8
import endpoints
from protorpc import message_types
from protorpc import messages
from protorpc import remote


class ParseRequest(messages.Message):
    content = messages.StringField(1)

class EchoResponse(messages.Message):
    """A proto Message that contains a simple string field."""
    content = messages.StringField(1)


PARSE_RESOURCE = endpoints.ResourceContainer(
    ParseRequest,
    utl_param = messages.StringField(1))

@endpoints.api(name='parse', version='v1', base_path='/api/')
class ParseApi(remote.Service):
    @endpoints.method(
        message_types.VoidMessage,
        EchoResponse,
        path='echo',
        http_method='GET',
        name='echo')
    def echo(self):
        return EchoResponse(message= 'pong')


    @endpoints.method(
        PARSE_RESOURCE,
        message_types.VoidMessage,
        path='source',
        http_method='POST',
        name='parse')
    def parse(self, request):
        return EchoResponse(message='to be parsed' )


    @endpoints.method(
        PARSE_RESOURCE,
        message_types.VoidMessage,
        path='checksource',
        http_method='POST',
        name='checksource')
    def check_parse(self, request):
        return EchoResponse(message='to be checked parsing ')


# [START api_server]
api = endpoints.api_server([ParseApi])
# [END api_server]