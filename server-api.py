# coding: utf-8   coding utf-8  coding=utf-8
import endpoints
from protorpc import message_types
from protorpc import messages
from protorpc import remote

from tools import tools
from Manager import Manager
from PackManager import PackManager

import FactoryParse
import os


class RequestBody(messages.Message):
	# POST Request body must contains this structure: {"timezone": "+3","time": "2008-12-12 19:21:10","locale": "ru","sourcehash":"1521528693","requestbody": "html"}
	timezone  = messages.StringField(1) # '+3' to UTC
	time = messages.StringField(2)	  # '2008-12-12 19:21:10'
	locale = messages.StringField(3)	# 'ru'
	sourcehash = messages.StringField(4)  # timestamp in seconds 1520847579
	requestbody  = messages.StringField(5) # html-string #to do: json-array
	

class Response(messages.Message):
	"""A proto Message that contains a simple string field."""
	# POST Responce body will contains this structure: 
	#  {"redirectlist":"['127.0.0.1','localhost']", sourcehash":"a3s4d2f8g0", content": "bla bla bla XY"}
	content  = messages.StringField(1)
	sourcehash  = messages.StringField(2)
	redirectlist = messages.StringField(3)
	supported_sources = messages.StringField(4)

PARSE_RESOURCE = endpoints.ResourceContainer(
	RequestBody,
	url_param = messages.StringField(1)) # twitter.com_JonnyBones

@endpoints.api(name='ParseApi', version='v1', base_path='/api/')
class ParseApi(remote.Service):
	@endpoints.method(
		message_types.VoidMessage,
		Response,
		path='echo',
		http_method='GET',
		name='echo')
	def echo(self, request):
		return Response(content = "pong")


	@endpoints.method(
		PARSE_RESOURCE,
		Response,
		path='parse/{url_param}',
		http_method='POST',
		name='parse')
	def parse(self, request):
		if (request.timezone==None):
			message = 'Broken Json. Failed to get timezone: %s ' % (request.timezone)
			raise endpoints.InternalServerErrorException(message)
		#client_ip = (self.request_state.remote_address if self.request_state else '') 
		client_ip = os.environ["REMOTE_ADDR"]
		
		manager_ = Manager(request.requestbody, request.url_param, request.time, request.timezone, request.locale, request.sourcehash, client_ip)
		json_news_list = tools.FinalJson(manager_.Parse())
		sourcehash= manager_.GetHashSource()
		ip_another_clients = manager_.GetIp()
		#return Response(content='{} {}'.format(request.requestbody, request.url_param))
		return Response(content=json_news_list, sourcehash=sourcehash, redirectlist=ip_another_clients)


	@endpoints.method(
		PARSE_RESOURCE,
		Response,
		path='parsepack',
		http_method='POST',
		name='parsepack')
	def parsepack(self, request):

		#TO DO
		
		client_ip = os.environ["REMOTE_ADDR"]
		
		packmanager_ = PackManager(request.requestbody, request.time, request.timezone, request.locale, client_ip)
		json_news_list = tools.FinalJson(packmanager_.Parse())
		sourcehash= "to do"
		ip_another_clients = "to do"
		return Response(content=json_news_list)


	@endpoints.method(
		message_types.VoidMessage,
		Response,
		path='supportedsources',
		http_method='GET',
		name='supportedsources')
	def supportedsources(self, request):
		supported = FactoryParse.GetSupportedAsStringOfList()
		return Response(supported_sources = supported)

	@endpoints.method(
		PARSE_RESOURCE,
		Response,
		path='checksource/{url_param}',
		http_method='POST',
		name='checksource')
	def checksource(self, request):
		check_result = FactoryParse.IsSupported(request.url_param)
		return Response(supported_sources=check_result)


# [START api_server]
api = endpoints.api_server([ParseApi])
# [END api_server]