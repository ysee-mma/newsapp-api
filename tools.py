# coding: utf-8   coding utf-8  coding=utf-8
from ParsingException import ParsingException
import json
import re

class tools(object):

	
	def EncodeJson(data):
		'''
		1 \ replace to \\
		2 replace " to \"
		3 replace </ to <\/
		4 add space before /> -- skip

		'''
		try:
			result = data
			result = result.replace('\\','\\\\')
			result = result.replace('"','\\"') # ???
			result = result.replace('</','<\/')
			
			
			return result
		except Exception as e1:
			message = 'An Error occurred while formatting json-data to valid json: "%s"' % (str(e1))
			ParsingException.GAE(message)
		

	def FinalJson(newsjson):
		try:
			return tools.EncodeJson(tools.safe_str(newsjson))
		except Exception, e:
			raise e
		return ""


	def JsonUnEscape(data):
		try:
			escaped = ""
			for i, c in enumerate(data):
				if (c=='u'):
					try:
						int(data[i+1:i+5],16) # вырезаем следующие четыре символа и пытаемся преобразовать в число 16ричное
						escaped = escaped + "\\"
					except Exception as e:
						pass
				escaped = escaped + c

			return escaped
			#return data.replace('u','\u').decode('unicode-escape')
#			return data.decode('unicode-escape')
			#return json.loads(data.decode('unicode-escape'))
			#return json.loads(data.replace('u','\u'))  # ошибка в случае если есть слово count например, тогда бьется co\unt и ломается
			
		except Exception as e1:
			#return data
			message = 'An Error occurred while unescaping json-string (example \u3059 to char): %s. raw data =[%s]' % (str(e1),data)
			ParsingException.GAE(message)

	def JsonEscape(data):
		try:
			return json.dumps(data)
		except Exception as e1:
			#return data
			message = 'An Error occurred while escaping json-string (char to \u3059): "%s"' % (str(e1))
			ParsingException.GAE(message)

	def ExtractDataFromJson(data):
		'''
		Undo from last step to first:
		1 \ replace to \\
		2 replace " to \"
		3 replace </ to <\/
		4 add space before /> -- skip

		'''
		try:

			result = data
			result = result.replace('<\/','</')
			result = result.replace('\\"','"')
			result = result.replace('\\\\','\\')
			return result
		except Exception as e1:
			message = 'An Error occurred while converting json-data to html-string: "%s"' % (str(e1))
			ParsingException.GAE(message)



	def safe_str(obj):
		try:
			#return str(obj)
			#return unicode(obj, 'utf_8')
			return obj.decode('utf-8')
		except UnicodeEncodeError:
			#return obj.encode('ascii', 'ignore').decode('ascii')
			#return obj.encode('ascii', 'replace').decode('ascii')
			return obj
			



	def GetUpTo2Domain(url_with_param):
		try:
			domain = ""
			if (url_with_param!=""):
				domain = url_with_param.lower()
				domain = domain.split('_')[0]
			if (domain):
				return domain
			else:
				return url_with_param
		except Exception, e:
			return url_with_param


	ExtractDataFromJson = staticmethod(ExtractDataFromJson)
	EncodeJson = staticmethod(EncodeJson)
	FinalJson = staticmethod(FinalJson)

	
	
	safe_str = staticmethod(safe_str)
	GetUpTo2Domain = staticmethod(GetUpTo2Domain)

	JsonUnEscape = staticmethod(JsonUnEscape)
	JsonEscape = staticmethod(JsonEscape)