# coding: utf-8   coding utf-8  coding=utf-8

import endpoints
import FactoryParse
from ParsingException import ParsingException
from tools import tools
from datetime import datetime, timedelta
from pytz import timezone
import pytz
import sys

class Manager(object):
	'''
	switching data between parsing modules 
	 
	'''
	this_module_name = str(sys.modules[__name__])
	count=-1
	Source_Name ="" # twitter.com_JonnyBones -> twitter.com
	Source_param = "" #JonnyBones
	data = "" # data to parse
	source_url = ""
	clienttime = "" # '2008-12-12 19:21:10'
	clienttimezone = "" # '+3' to UTC
	clientlocale = "" # 'ru'
	clientip = ""
	
	articles = []

	SourcesAndHashesDictionary = {}
	HashesAndJsonDictionary = {}
	HashesAndIPDictionary = {} #'hash_of_twitter.com_JonnyBones':['127.0.0.1','localhost']}


	response_json_news_temp =""
	response_sourcehash_temp = ""
	response_json_news_temp = ""


	sourcehash_magic_always_not_valid = "1520842430" # UTC 'Mar 12 2018 08:13:50' since Unix-Epoch in seconds as string
	source_parsing_delay_in_seconds = 2*60
	epoch = datetime(1970, 1, 1,tzinfo=None)#,tzinfo=pytz.utc)# tzinfo=timezone('UTC')) #tzinfo=timezone.utc)

	ip_another_clients = ""

	
	
	def __init__(self, data_to_parse, source, time,timezone,locale,sourcehash_client, client_ip):
		'''
		call from API: Manager(request.requestbody, request.url_param, request.time, request.timezone, request.locale, request.sourcehash)
		Constructor
		'''

		self.source_url = source
		self.clienttime = time
		self.clienttimezone = timezone
		self.clientlocale = locale
		self.clientip = client_ip

		self.Source_Name = tools.GetUpTo2Domain(source)

		self.CheckRepeatedRequest(source, sourcehash_client, client_ip)

		self.data = tools.ExtractDataFromJson(tools.safe_str(data_to_parse))




	def CheckRepeatedRequest(self, source, sourcehash_client, client_ip):
		'''
			Если на сервере есть действующий хэш источника,
			то возвращаем соответствующий ему (json или ip других клиентов) от предыдущего парсинга.
			Если хэш устарел или json утерян, то парсим данные заново и обновляем словари хэша и json
		'''
		this_function_name = str(sys._getframe().f_code.co_name)
		self.response_json_news_temp = ""
		try:
			if (source in self.SourcesAndHashesDictionary):
				sourcehash_server = self.SourcesAndHashesDictionary[source]

				self.response_sourcehash_temp = sourcehash_server
				if (self.IsSourceHashValid(sourcehash_server,sourcehash_client)):
					# no parsing, just send (previous json OR/AND ip's of another clients)
					
					try:
						if (sourcehash_server in self.HashesAndJsonDictionary):
						
							self.response_json_news_temp = self.HashesAndJsonDictionary[sourcehash_server]
					
						if (sourcehash_server in self.HashesAndIPDictionary):
						
							self.ip_another_clients = self.HashesAndIPDictionary[sourcehash_server]
					
						if (not client_ip in self.HashesAndIPDictionary[sourcehash_server]):
						
							self.HashesAndIPDictionary[sourcehash_server].append(client_ip)

					except Exception, e1:
						message = 'parsing manager unable to manage repeated request with hash: "%s" for source %s with error %s at %s %s' % (sourcehash_client, source,str(e1), this_function_name, self.this_module_name)
						ParsingException.GAE(message)

				else:
					pass # full parsing, update hash dict
			else:
				# first request to this source
				self.response_sourcehash_temp = self.GenerateHash()

				self.SourcesAndHashesDictionary.update({source:self.response_sourcehash_temp})

		except Exception, e:
			message = 'parsing manager unable to manage given hash: "%s" for source %s with error %s at %s %s' % (sourcehash_client, source,str(e), this_function_name, self.this_module_name)
			ParsingException.GAE(message)	



	def GenerateHash(self):
		this_function_name = str(sys._getframe().f_code.co_name)
		try:
			#date_now =  datetime.now(tz=timezone('UTC'))#date_now =  datetime.now(tzinfo=timezone('UTC'))			#date_now =  datetime.utcnow().replace(tzinfo=pytz.utc)
			date_now =  datetime.now(tz=None)

			timestamp = (date_now - self.epoch)# // timedelta(seconds=1)
			timestamp = long(timestamp.total_seconds())
			
			
			return str(timestamp)
		except Exception, e:
			message = 'An error occurred while generating hash. %s at %s %s' % (str(e),this_function_name, self.this_module_name)
			ParsingException.GAE(message)


	def GetHashSource(self):
		return self.response_sourcehash_temp

	def GetIp(self):
		#ip_as_str = ', '.join('%s' %(k) for k in ip_another_clients)
		#dict_ip_print = ', '.join('%s : %s' %(k,self.HashesAndIPDictionary[k]) for k in self.HashesAndIPDictionary.keys())
		ip_as_str = ', '.join(self.ip_another_clients)
		return ip_as_str



	def IsSourceHashValid(self, sourcehash_server, sourcehash_client):
		'''
		Проверяем актуальность хэша
		магический тайный хэш будет проваливть проверку, чтобы начать честный парсинг с нуля
		чтобы отфутболить клиента- отдаем True
		чтобы парсить по-честному - отдаем False
		'''
		this_function_name = str(sys._getframe().f_code.co_name)
		try:
			if (sourcehash_client==self.sourcehash_magic_always_not_valid):
				return False
			if (not sourcehash_client) or (sourcehash_client==''):
				return False

			#date_client_hash = long(int(sourcehash_client))
			date_client_hash = long(sourcehash_client)
			#date_now = datetime.now(tz=timezone('UTC'))			#datetime.utcnow()			#date_now =  int(datetime.now().strftime("%s")) * 1000 			#date_now = (datetime.now(tz=timezone('UTC')) - datetime(1970, 1, 1)).total_seconds()
			date_now_in_secs = long((datetime.now(tz=None) - self.epoch).total_seconds())

			if (date_now_in_secs-date_client_hash>self.source_parsing_delay_in_seconds):
				return False

			return True
		except Exception, e:
			message = 'An error occurred while checking validation of hash: client ["%s"] server [%s] . %s at %s %s' % (sourcehash_client,sourcehash_server,str(e),this_function_name, self.this_module_name)
			ParsingException.GAE(message)
		return False
	


	def Parse(self):
		this_function_name = str(sys._getframe().f_code.co_name)
		status =''
		try:
			try:
				status = 'next step: repeat check'
				if (self.response_json_news_temp):
					#   не пустой response_json_news_temp. 
					#   Мы подготовили прошлый валидный json в init, 
					#   нет смысла парсить заново, возвращаем его
					return tools.FinalJson(self.response_json_news_temp)
			except Exception, e1:
				message = 'failed to check repeate of previous request: "%s" . %s' % (self.Source_Name,str(e1))
				ParsingException.GAE(message)

			# парсим с нуля по-честному
			try:
				status = 'next step: defining source by name and parsing'
				parser = FactoryParse.GetSourceByName(self.Source_Name)(self.data,self.source_url, self.clienttime, self.clienttimezone, self.clientlocale)

			except Exception, e1:
				message = 'failed to send data to specific parsing module: "%s" . %s' % (self.Source_Name,str(e1))
				ParsingException.GAE(message)


			try:
				status = 'next step: final json collecting'
				self.count = parser.count

				self.HashesAndJsonDictionary.update({self.response_sourcehash_temp : tools.FinalJson(parser.articles_parsed_as_json)})

				self.HashesAndIPDictionary.update({self.response_sourcehash_temp:self.clientip})

				return parser.articles_parsed_as_json

			except Exception as e1:
				message = 'parsing manager unable to update Dictionaries: "%s" . %s at %s' % (self.Source_Name,str(e1), this_function_name)
				ParsingException.GAE(message)
		except Exception as e1:
			message = 'parsing manager unable to parse: "%s" .status [%s] %s at %s' % (self.Source_Name, status,str(e1), this_function_name)
			ParsingException.GAE(message)









