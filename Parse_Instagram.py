# -*- coding: utf-8 -*-
import sys
import json
from Parse_blank import Parse_blank_class
from ParsingException import ParsingException
from tools import tools
from Article import Article



class Parse_Instagram(Parse_blank_class):
    '''
    parsing instagram.com articles
    
    '''
    this_module_name = str(sys.modules[__name__])
    url_domain = "instagram.com"
    
    user = ""
    
    prefix = u"window._sharedData = ";
    postfix = ";</script>";
    
    

    #*********************************************************************************************************************************************************************************************************
    
    def ParseInstagram(self):
        this_function_name = str(sys._getframe().f_code.co_name)

        self.getUserName()

        posts = self.find_Articles(self.parse_data)
        
        try:
            articles = []
            self.count = len(posts)
            for post in posts:
                str_post_date = self.find_Date(post)
                date_as_long = self.convertToMilliseconds(str_post_date)
                date_as_date = self.getDateFromMilliseconds(date_as_long)
                title = self.find_Title(post)
                pic = self.find_Pic(post)
                link = self.find_Link(post)
                
                art = Article(title,link,pic,date_as_long,date_as_date, "", self.source_type)
                #art = self.Article_Module.Article(title,link,pic,date_as_long,date_as_date, "", self.source_type)
                    
                self.articles_parsed.append(art)

                self.AddArticleToJson(art)

                self.UpdateLastPostDate(art)
                    
            
            self.articles_parsed_as_json = self.articles_parsed_as_json + "]"
                
            #sys.stdout.write("распарсили статей %s " %self.count)
            #sys.stdout.write(" источник: %s \n" %self.url)
            #sys.stdout.flush()
        except Exception as e:
            #sys.stdout.write("Ошибка выделения элементов статьи. %s\n" %e )
            #sys.stdout.flush()
            self.articles_parsed_as_json = self.articles_parsed_as_json + "]"
            message = "An Error occurred while selecting each articles." + str(json.dumps(e))+  " "+ self.this_module_name+ " "+ this_function_name
            ParsingException.GAE(message)  
    
    
    def getUserName(self):
        this_function_name = str(sys._getframe().f_code.co_name)
        try:
            self.user = self.url.split('instagram.com_')[1]
        except Exception as e:
            message = "An Error occurred while defining username from the url. Tried to divide given string [%s] by fragnent 'instagram.com_' looking for element number 1" + str(e)+ " "+ self.this_module_name +" "+ this_function_name  % (str(self.url))
            ParsingException.GAE(message)
            #self.LogError("Не смогли выделить имя пользователя на странице."+ str(e)+"\n", str(self.url)+ self.this_module_name)
            #sys.stdout.write("ошибка определения владельца Instagram. %s\n" %E )
            #sys.stdout.flush()


    def find_values_in_json(self,id, json_repr):
        # text = self.find_values_in_json('text', json_part)
        results = []

        def _decode_dict(a_dict):
            try: 
                if (a_dict[id] is not None):
                    results.append(a_dict[id])
            except KeyError : pass
            return a_dict
        try:
            json.loads(json_repr, object_hook=_decode_dict)  # Return value ignored.
            return results
        except Exception as e:
            pass


    def find_Articles(self, parse_data):
        this_function_name = str(sys._getframe().f_code.co_name)
        jsonselector= 'node'
        try:
            #status = "before init BS4"
            #ParsingException.GAE(status)
            #soup = BeautifulSoup(self.parse_data, 'html.parser',from_encoding="utf-8")
            #selector = "body"
            #res = []
            #status = "before select"
            #res = soup.select(selector)

            #status = "after select"
            #message = status + str(res)
            #ParsingException.GAE(status)

            json_part = self.parse_data.split(self.prefix, 1)[1]
            json_part = (json_part.split(self.postfix, 1)[0])
            json_part = str(json_part).replace("b'", "").replace("\'", "").replace("\\", "").replace("'", "")

        except Exception as e:
            message = "Failed to find news container from ["+self.prefix+"] to ["+self.postfix+"]. with error: %s at %s %s " % (str(e),this_function_name,self.this_module_name)
            #message = "Ошибка выделения куска json-кода на html-странице, в котором передаются посты.Искали от ["+self.prefix+"] до ["+self.postfix+"]." + str(e)+ " "+ str(parse_data)+ " "+ self.this_module_name
            ParsingException.GAE(message)
            #self.LogError("Не смогли выделить кусок json-кода на html-странице. Искали от ["+self.prefix+"] до ["+self.postfix+"]."+ str(e)+"\n", str(parse_data)+ self.this_module_name)
            #sys.stdout.write("ошибка парсинга Instagram. Не смогли выделить Json-кусок на странице. %s\n" %E )
            #sys.stdout.flush()
        
        nodes =[]
        try:
            
            nodes = self.find_values_in_json(jsonselector, tools.JsonUnEscape(json_part))

        except Exception as e:
            message = "An Error occurred while finding article-nodes in json. " + str(e)+ " "+ str(json_part)+ " "+ self.this_module_name + " "+this_function_name
            ParsingException.GAE(message)

        try:
            leafs =[]
            for node in nodes:
                leafs=self.find_values_in_json(jsonselector, json.dumps(node))
                if ((leafs is None) or (len(leafs)==0)):
                    nodes.remove(node)

        except Exception as e:
            message = "An Error occurred while clearing article-nodes. " + str(e)+ " "+ str(nodes)+ " "+ self.this_module_name + " "+this_function_name
            ParsingException.GAE(message)
        
        return nodes
# dead code below


        try:
            json_obj = json.loads(json_part)
        except Exception as e:
            message = "An Error occurred while translating json from html to json-object. " + str(e)+ " "+ str(json_part)+ " "+ self.this_module_name + " "+this_function_name
            ParsingException.GAE(message)
            #self.LogError("Не смогли из json-кода на html-странице создать json -Объект."+ str(e)+" \n", str(json_part)+ self.this_module_name)
            #sys.stdout.write("ошибка преобразования в json. %s\n" %E )
            #sys.stdout.flush()
            
        selector =['entry_data', 'ProfilePage', 'user', 'media', 'nodes']
        selector2 =['entry_data', 'ProfilePage', 'graphql','user', 'edge_owner_to_timeline_media', 'edges','node']
        try:
            #sys.stdout.flush()
            #posts = json_obj[selector[0]][selector[1]][0][selector[2]][selector[3]][selector[4]]
            e1 = json_obj[selector2[0]][selector[1]]
            ParsingException.GAE("selector["+selector2[0]+"."+selector[1]+"] count: "+str(len(e1)))

            [selector2[1]][0][selector2[2]][selector2[3]][selector2[4]][selector2[5]][selector2[6]]
            
            
            if len(posts==0):
                ParsingException.GAE("An Error. Empty list after selecting posts by selector ["+str(selector)+"] with this sourcecode 'posts = json_obj[selector[0]][selector[1]][0][selector[2]][selector[3]][selector[4]]'. " + str(e)+ " json=["+ str(json_obj)+ "] "+ self.this_module_name)+" "+this_function_name

            ParsingException.GAE("test after selecting posts")    
            return posts
        except Exception as e:
            message = "An Error occured while selecting posts by selector["+str(selector)+"] with this sourcecode'posts = json_obj[selector[0]][selector[1]][0][selector[2]][selector[3]][selector[4]]'. " + str(e)+ " "+ str(json_obj)+ " "+ self.this_module_name + " "+this_function_name
            ParsingException.GAE(message)
            #self.LogError("Не смогли в json -Объекте найти ни одной записи. Селектор ["+selector+"]."+ str(e)+" \n", str(json_obj)+ self.this_module_name)
            #sys.stdout.write("Не найдена ни одна запись: . %s\n" %self.url )
            #sys.stdout.write("Ошибка: . %s\n" %E )
            #sys.stdout.flush()   
            
         
    def find_Pic(self, post):
        selector = 'display_src'

        this_function_name = str(sys._getframe().f_code.co_name)
        jsonselector = 'display_url'
        try:
            pics = []
            pics = self.find_values_in_json(jsonselector, json.dumps(post))
            return pics[0]
        except Exception as e1:
            message = "An Error occurred while selecting Pics of post by selector ["+jsonselector+"]. " + str(e1)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)


            try:
                res= (str(post[selector]).split('.jpg?', 1)[0]+".jpg")
                if not res:
                    message = "An Error occurred while selecting Pic by selector ["+selector+"]. Empty list" , str(post)+ " "+ self.this_module_name + " "+this_function_name
                    ParsingException.GAE(message)
                return res
            except Exception as e:
                message = "An Error occurred while selecting Pic by selector ["+selector+"]. " + str(e)+ " "+ str(post)+ " "+ self.this_module_name+ " "+this_function_name
                ParsingException.GAE(message)
                #self.LogError("Не смогли выделить картинку статьи на странице.  Селектор ["+selector+"]."+ str(e)+"\n", str(post)+ self.this_module_name)
                return "" 
        
        
    def find_Link(self, post):
        selector = 'code'
        this_function_name = str(sys._getframe().f_code.co_name)
        jsonselector = 'shortcode'
        try:
            Links = []
            Links = self.find_values_in_json(jsonselector, json.dumps(post))
            res=("/p/"+Links[0] + '/')
            return res
        except Exception as e1:
            message = "An Error occurred while selecting Link of post by selector ["+jsonselector+"]. " + str(e1)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)

# dead code below

        try:
            '''
            исключим "https://www."+self.url_domain из экономии трафика
            res=("https://www."+self.url_domain+"/p/"+post[selector] + '/')
            '''
            res=("/p/"+post[selector] + '/')
            if not res:
                message = "An Error occurred while selecting Link by selector ["+selector+"]. Empty list" , str(post)+ " "+ self.this_module_name+" "+this_function_name
                ParsingException.GAE(message)
            return res
        except Exception as e:
            message = "An Error occurred while selecting Link by selector ["+selector+"]. " + str(e)+ " "+ str(post)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)
            #self.LogError("Не смогли выделить ссылку статьи на странице.  Селектор ["+selector+"]."+ str(e)+"\n", str(post)+ self.this_module_name)
            return "" 
            
        
        
    def find_Date(self, post):
        # return date in seconds as int in string 
        this_function_name = str(sys._getframe().f_code.co_name)
        jsonselector = 'taken_at_timestamp'
        selector = 'date'
        try:
            dates = []
            dates = self.find_values_in_json(jsonselector, json.dumps(post))
            return dates[0]
        except Exception as e1:
            message = "An Error occurred while selecting datetime of post by selector ["+jsonselector+"]. " + str(e1)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)

# dead code below

        try:
            res =str(post[selector])
            if not res:
                message = "An Error occurred while selecting Datetime of post by selector ["+selector+"]. Empty result" , str(post)+ " "+ self.this_module_name+" "+this_function_name
                ParsingException.GAE(message)
            return res
        except Exception as e:
            message = "An Error occurred while selecting datetime of by selectorу ["+selector+"]. " + str(e)+ " "+ str(post)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)
            #self.LogError("Не смогли выделить дату статьи на странице.  Селектор ["+selector+"]."+ str(e)+"\n", str(post)+ self.this_module_name)
            return ""
        
        
        
    def find_Title(self, post):
        selector = 'caption'
        jsonselector = 'text'

        this_function_name = str(sys._getframe().f_code.co_name)
        try:
            titles = []
            titles = self.find_values_in_json(jsonselector, json.dumps(post))
            return tools.JsonUnEscape(titles[0])
        except Exception as e1:
            message = "An Error occurred while selecting Title by selector ["+jsonselector+"]. " + str(e1)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)



        try:
            res = (self.user+" : '"+str(post[selector]) +"'")
            if not res:
                message = "An Error occurred while selecting Title by selector ["+selector+"]. Empty title", str(post)+ " "+ self.this_module_name+" "+this_function_name
                ParsingException.GAE(message)
            return res
        except Exception as e:
            message = "An Error occurred while selecting Title by selector ["+selector+"]. " + str(e)+ " "+ str(post)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)
            #self.LogError("Не смогли выделить заголовок статьи на странице.  Селектор ["+selector+"]."+ str(e)+"\n", str(post)+ self.this_module_name)
            return ""
    
    def convertToMilliseconds(self,date_sec):
        '''
        return 1000*Long.parseLong(  Math.round( Double.parseDouble( _Date ))+"");
        '''
        try:
            return 1000*int(date_sec)
        except Exception as e:
            message = "An Error occurred while translating seconds to milliseconds. " + str(e)+ " "+ str(date_sec)+ " "+ self.this_module_name+" "+this_function_name
            ParsingException.GAE(message)
            return 0
        
            

    
    
    