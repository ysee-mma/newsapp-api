# -*- coding: utf-8 -*-# coding utf-8
from bs4 import BeautifulSoup
from datetime import datetime ,timedelta
from pytz import timezone
import sys
import Logging
import FactoryParse
from Article import Article
from ParsingException import ParsingException
import endpoints
from tools import tools

class Parse_blank_class(object):
	'''
	classdocs
	'''
	this_module_name = str(sys.modules[__name__])

	count =-2
	
	url_domain = ""
	clienttime = ""
	clientdatetime = datetime.now(tz=timezone('UTC')) # will be owerwrited to '2008-12-12 19:21:10'
	clientdatetime_format = '%Y-%m-%d %H:%M:%S' # 2008-12-12 19:21:10",
	clienttimezone = ""
	clientlocale = ""
	
	Article_Module_name = "Article"
	Article_Module = [None]

	articles_parsed=[]
	articles_parsed_as_json = "["
	last_post = 0
	
	source_type = "unknown"
	
	url = ""
	
	parse_data = ""

	def __init__(self, parse_data,url,time,timezone,locale):
		'''
		Constructor
		'''

		this_function_name = str(sys._getframe().f_code.co_name)
		
		count = 0
		last_post = 0
		
		try:
			self.parse_data = tools.safe_str(parse_data)
			self.clienttime = time
			self.clienttimezone = timezone
			self.clientlocale = locale
			self.url = url
		except Exception, e:
			message = 'failed to initialize blank parsing module (error=%s) url=%s time=%s timezone=%s locale=%s data=[%s]. In %s at %s' % (str(e),url,time,timezone,locale,parse_data,this_function_name,self.this_module_name)
			ParsingException.GAE(message)

		
		try:
			self.clientdatetime = datetime.strptime(self.clienttime, self.clientdatetime_format)
		except Exception, e:
			message = 'failed to parse given time: "%s" as %s. %s. In %s at %s' % (time,self.clientdatetime_format,str(e),this_function_name,self.this_module_name)
			ParsingException.GAE(message)
			'''
		else:
			pass
		finally:
			message = 'successfully parsed given time: "%s" as %s' % (time,self.clientdatetime_format)
			ParsingException.GAE(message)
			'''

		try:
			self.source_type = FactoryParse.GetType(self.url_domain)
		except Exception, e:
			message = 'failed to defining the type of url_domain: "%s" in %s at %s' % (self.url_domain,this_function_name,self.this_module_name)
			ParsingException.GAE(message)
		

		if ("instagram" not in self.url):
			try:

				#str.find(str, beg=0, end=len(string))

				soup = BeautifulSoup(self.parse_data, 'html.parser',from_encoding="utf-8")
				#soup = BeautifulSoup(self.parse_data, 'html.parser',from_encoding="ascii")


				articles = []
				
				
				articles = self.find_Articles(soup) 

				self.count = len(articles)
				
				for article in articles:

					art = self.ParseAndBuildArticleObject(article)
					
					self.articles_parsed.append(art)

					self.AddArticleToJson(art)

					self.UpdateLastPostDate(art)
					
					#sys.stdout.write(" создали объект-Article: %s \n"  %str(art.GetJSON()))
					#sys.stdout.flush()
								
				self.articles_parsed_as_json = self.articles_parsed_as_json + "]"				
				
				##Logging.Log.Write("распарсили статей " + str(self.count) )
			except Exception as e:
				#message = "Ошибка проверки каждой найденной статьи от источника url ["+url+"] и составления json-объекта из них."+str(e)+" \n", str(self.parse_data), str(sys.modules[__name__])
				message = "An Error occurred while checking every finded article from source url ["+url+"] and building json. "+str(e)+" "+ this_function_name+" "+self.this_module_name+"\n"
				ParsingException.GAE(message)
				#self.LogError(message)
				#Logging.Log.Write("ошибка парсинга внутри модуля: " + str(e) )
				self.articles_parsed_as_json = self.articles_parsed_as_json + "]"  
		else:  
			self.ParseInstagram()	

		
	def ParseAndBuildArticleObject(self, article_unparsed):
		this_function_name = str(sys._getframe().f_code.co_name)
		try:
			try:
				title = self.find_Title(article_unparsed)
			except Exception, e:
				message = "An Error occurred while find_Title  . %s at %s" % (str(e), this_function_name)
				ParsingException.GAE(message)
			
			try:
				date  = self.find_Date(article_unparsed) 
				link = self.find_Link(article_unparsed)
				pic = self.find_Pic(article_unparsed)
				date_as_long = self.get_Date_as_Long(date)
				date_as_date = self.getDateFromMilliseconds(date_as_long)
			except Exception, e:
				message = "An Error occurred while find_Date find_Link find_Pic get_Date_as_Long getDateFromMilliseconds. %s at %s" % (str(e), this_function_name)
				ParsingException.GAE(message)
			
			art = Article(title,link,pic,date_as_long,date_as_date, "", self.source_type)
		except Exception, e:
			message = "An Error occurred while building Article-object. %s at %s" % (str(e), this_function_name)
			ParsingException.GAE(message)
		return art



	def AddArticleToJson(self,article_obj):
		this_function_name = str(sys._getframe().f_code.co_name)
		try:
			if ( self.last_post==0 ):
				self.articles_parsed_as_json = self.articles_parsed_as_json + article_obj.GetJSON()
			else:
				self.articles_parsed_as_json = self.articles_parsed_as_json + ", " + article_obj.GetJSON()
		except Exception, e:
			message = "An Error occurred while adding new article in raw json. "+str(e)+" "+ this_function_name+" "+self.this_module_name+"\n"
			ParsingException.GAE(message)


	def UpdateLastPostDate(self, article_obj):
		this_function_name = str(sys._getframe().f_code.co_name)
		try:
			if (article_obj.GetDateAsLong() > self.last_post):
				self.last_post = article_obj.GetDateAsLong()
		except Exception, e:
			message = "An Error occurred while updating last (newest) article date. "+str(e)+" "+ this_function_name+" "+self.this_module_name+"\n"
			ParsingException.GAE(message)


	def getArticles (self):
		return self.articles_parsed
		  
	def getLastPost (self):
		return self.last_post
	

	#*********************************************************************************************************************************************************************************************************
	
	def ParseInstagram(self):
		'''
		Перезаписывается в модуле инстаграма
		'''
		pass
	
	def find_Articles(self, soup):
		'''
		Перезаписывается для каждого модуля парсинга. Пример:
		return soup.select("div[id=BuzzerListContainer] article")
		'''
		pass
		 
	def find_Pic(self, article):
		'''
		Перезаписывается для каждого модуля парсинга. Пример:
		try:
			pic = article.select("img")[0].get("src");
			try:
				pic = pic.split('.vresize')[0]+".jpg"
			except Exception as e:
				pass
			return pic
		except Exception as e:
			return ""
		'''
		pass
		
	def find_Link(self, article):
		'''
		Перезаписывается для каждого модуля парсинга. Пример:
		try:
			link = article.select("a[class=buzzer-title-link]")[0].get("href"); # http://www.foxsports.com/ufc/story/ufc-hidalgo-poirier-vs-johnson-post-fight-thoughts-091816
это исключим для экономии трафика if self.url_domain not in link:
это исключим для экономии трафика link = self.url_domain+link
			
это исключим для экономии трафика if "http://www." not in link:
это исключим для экономии трафика link = "http://www."+link 
			return link
		except Exception as e:
			return ""
		'''
		pass
		
		
	def find_Date(self, article):
		'''
		Перезаписывается для каждого модуля парсинга. Пример:
		try:
			date = article.select("span[class=buzzer-pubdate]")[0].get_text() # SEP 17, 10:06p ET -- SEP 18, 1:01a ET 
			return date
		except Exception as e:
			return ""
		'''
		pass
		
		
	def find_Title(self, article):
		'''
		Перезаписывается для каждого модуля парсинга. Пример:
		try:
			title = article.select("h3")[0].get_text()
			return title
		except Exception as e:
			return "" 
		'''
		pass
	
	
		#*********************************************************************************************************
		
		
	def getDateFromMilliseconds(self,ms):
		this_function_name = sys._getframe().f_code.co_name
		try:
			return datetime.fromtimestamp(ms//1000.0)
		except Exception as e:
			message = "An Error Occurred while converting given Object to DateTime-Object. "+str(e)+" "+this_function_name+ " " +str(sys.modules[__name__])
			ParsingException.GAE(message)
		
	def set_Year(self, date):
		'''
		Дополняем дату годом. 
		Если сейчас январи и номер дня меньше чем в дате, то год уменьшаем
		'''
		this_function_name = sys._getframe().f_code.co_name
		try:
			now_year = datetime.today().year
			now_month = datetime.today().month
			now_day = datetime.today().day
			
			if ((now_month==1)&(now_day<date.utctimetuple().tm_mday)):
				date = date.replace(year=now_year-1)
			else:
				date = date.replace(year=now_year)
			
			return date
		except Exception as e:
			message =  "An Error occurred while setting year to the current date. "+str(e)+" "+this_function_name+ " " +str(sys.modules[__name__])
			ParsingException.GAE(message)
			#message = "Ошибка добавления в дату года в зависимости от переданной и текущей даты."+str(e)+" \n", str(date), str(sys.modules[__name__])
		
	
	def get_Milliseconds(self, dt):
		this_function_name = sys._getframe().f_code.co_name
		try:
			epoch = datetime(1970, 1, 1, tzinfo=timezone('UTC')) #tzinfo=timezone.utc)
			integer_timestamp = (dt - epoch) // timedelta(seconds=1)*1000
		except Exception as e1:
			message =  "An Error occurred while converting time to milliseconds "+str(e1)+" "+this_function_name+ " " +str(sys.modules[__name__])
			ParsingException.GAE(message)
			#message ="Ошибка получения даты в виде миллисекунд."+e1+" \n", str(dt), str(sys.modules[__name__])
			#Logging.Log.Write("ошибка перевода даты в миллисекунды: " + str(e1) )
			return 0 
		return integer_timestamp
		

	def get_count(self):
		return self.count
	
	
	def LogError(self,ErrorMessage, data, modulename):
		ParsingException.GAE(ErrorMessage)
		

	
	

	
	
	
			